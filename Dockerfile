FROM python:3.8-alpine

RUN apk --no-cache add wget
RUN pip3 install s3cmd==2.1.0

CMD ["/usr/local/bin/s3cmd"]
